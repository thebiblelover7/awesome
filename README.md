# My AwesomeWM Config

![Screenshot](https://gitlab.com/thebiblelover7/awesome/-/raw/master/Screenshot.png)

To clone:
```
git clone https://github.com/adrielsand/awesome.git ~/.config/awesome
```

|                                  Function |              Shortcut |
|:------------------------------------------|:----------------------|
| Show hotkeys                              | Super + F1            |
| Search and open an app                    | Super + R             |
| Change workspaces                         | Super + 1-7           |
| Move windows to a workspace               | Super + Shift + 1-7   |
| Switch to previous workspace              | Super + Esc           |
| Switch windows                            | Super + Tab           |
| Switch windows (reverse)                  | Super + Shift + Tab   |
| Close a window                            | Super + Q             |
| Maximize a window                         | Super + F             |
| Change layout                             | Super + Space         |
| Change layout (reverse)                   | Super + Shift + Space |
| Move window position                      | Super + MouseClick    |
| Resize window                             | Super + Shift + MouseClick |
| Move window to next screen                | Super + O             |
| Minimize all windows in current workspace | Super + D             |
| Open default app for current workspace    | Super + T             |
| Open a browser                            | Super + B             |
| Open a file manager                       | Super + Shift + E     |
| Open an audio manager                     | Super + Shift + A     |
| Open an OBS window                        | Super + Shift + S     |
